/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import webgl1Test_webgl1 from './WebGL.test01.js'
import webgl1Test_webgl2 from './WebGL.test02.js'
import webgl1Test_webgl3 from './WebGL.test03.js'
import webgl1Test_webgl4 from './WebGL.test04.js'
import webgl1Test_webgl5 from './WebGL.test05.js'
import webgl1Test_webgl6 from './WebGL.test06.js'
import webgl1Test_webgl7 from './WebGL.test07.js'
import webgl1Test_webgl8 from './WebGL.test08.js'
import webgl1Test_webgl9 from './WebGL.test09.js'
import webgl1Test_webgl10 from './WebGL.test10.js'
import webgl1Test_webgl11 from './WebGL.test11.js'
import webgl1Test_webgl12 from './WebGL.test12.js'
import webgl1Test_webgl13 from './WebGL.test13.js'
import webgl1Test_webgl14 from './WebGL.test14.js'
import webgl1Test_webgl15 from './WebGL.test15.js'

export default function testsuite(){
	webgl1Test_webgl1()
	webgl1Test_webgl2()
	webgl1Test_webgl3()
	webgl1Test_webgl4()
	webgl1Test_webgl5()
	webgl1Test_webgl6()
	webgl1Test_webgl7()
	webgl1Test_webgl8()
	webgl1Test_webgl9()
	webgl1Test_webgl10()
	webgl1Test_webgl11()
	webgl1Test_webgl12()
	webgl1Test_webgl13()
	webgl1Test_webgl14()
	webgl1Test_webgl15()
}